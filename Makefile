REGISTRY=869575750609.dkr.ecr.us-east-1.amazonaws.com
IMAGE=osmosv2-merchant-frontend
IMAGE_TAG=latest
IMAGE_PORT=80

.DEFAULT: all

all: build run

release: build tag push

build:
	@echo "[INFO] Building $(IMAGE):$(IMAGE_TAG) docker image"
	docker build -t $(IMAGE):$(IMAGE_TAG) .

run:
	@echo "[INFO] Running $(IMAGE):$(IMAGE_TAG) docker image"
	docker run \
		--rm \
		--detach=true \
		--name $(IMAGE) \
		--publish $(IMAGE_PORT):$(IMAGE_PORT) \
		$(IMAGE)

tag:
	@echo "[INFO] Tagging $(IMAGE) docker image to $(IMAGE_TAG)"
	docker tag $(IMAGE):$(IMAGE_TAG) $(REGISTRY)/$(IMAGE):$(IMAGE_TAG)

push:
	@echo "[INFO] Pushing $(REGISTRY)/$(IMAGE)/$(IMAGE_TAG)"
	bash -c 'aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 869575750609.dkr.ecr.us-east-1.amazonaws.com'
	docker push $(REGISTRY)/$(IMAGE):$(IMAGE_TAG)

clean:
	@echo "[INFO] Removing $(IMAGE):$(IMAGE_TAG) docker image"
	docker rmi $(IMAGE):$(IMAGE_TAG)
	docker rmi $(REGISTRY)/$(IMAGE):$(IMAGE_TAG)
