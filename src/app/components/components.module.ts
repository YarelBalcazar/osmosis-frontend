import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent as AuthHeaderComponent } from './auth/header/header.component';
import { FooterComponent as AuthFooterComponent } from './auth/footer/footer.component';
import { HeaderComponent as Auth2HeaderComponent } from './auth2/header/header.component';
import { FooterComponent as Auth2FooterComponent } from './auth2/footer/footer.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [
    AuthFooterComponent,
    AuthHeaderComponent,
    Auth2FooterComponent,
    Auth2HeaderComponent
  ],
  exports: [
    AuthFooterComponent,
    AuthHeaderComponent,
    Auth2FooterComponent,
    Auth2HeaderComponent,
  ],
  providers: []
})
export class ComponentsModule { }
