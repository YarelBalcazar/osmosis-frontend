import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { Auth2LayoutComponent } from './layouts/auth2-layout/auth2-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/sign-in',
    pathMatch: 'full',
  }, {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren : () => import('./layouts/auth-layout/auth-layout.module').then(m => m.AuthLayoutModule)
      }
    ]
  },{
    path: 'auth2',
    component: Auth2LayoutComponent,
    children: [
      {
        path: '',
        loadChildren : () => import('./layouts/auth2-layout/auth2-layout.module').then(m => m.Auth2LayoutModule)
      }
    ]
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'auth/sign-in',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    relativeLinkResolution: 'legacy',
    useHash: false,

  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
