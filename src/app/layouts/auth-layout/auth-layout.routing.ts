import { Routes } from '@angular/router';

export const AuthLayoutRoutes: Routes = [
    { path: 'sign-up', loadChildren: () => import('../../pages/sign-up/sign-up.module').then(m => m.SignUpModule) },
    // tslint:disable-next-line: max-line-length
    { path: 'create-password', loadChildren: () => import('../../pages/create-password/create-password.module').then(m => m.CreatePasswordModule) },
    { path: 'reset-password', loadChildren: () => import('../../pages/reset-password/reset-password.module').then(m => m.ResetPasswordModule) },
    { path: 'find-my-url', loadChildren: () => import('../../pages/find-my-url/find-my-url.module').then(m => m.FindMyUrlModule) },
    { path: 'sign-in', loadChildren: () => import('../../pages/sign-in/sign-in.module').then(m => m.SignInModule) },
];
