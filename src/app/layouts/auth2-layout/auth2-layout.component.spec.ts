import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Auth2LayoutComponent } from './auth2-layout.component';

describe('Auth2LayoutComponent', () => {
  let component: Auth2LayoutComponent;
  let fixture: ComponentFixture<Auth2LayoutComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Auth2LayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Auth2LayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
