import { Routes } from '@angular/router';

export const Auth2LayoutRoutes: Routes = [
    { path: 'account-suspended', loadChildren: () => import('../../pages/account-suspended/account-suspended.module').then(m => m.AccountSuspendedModule) },
    { path: 'free-trial-over', loadChildren: () => import('../../pages/free-trial-over/free-trial-over.module').then(m => m.FreeTrialOverModule) },
    { path: 'onboarding', loadChildren: () => import('../../pages/onboarding/onboarding.module').then(m => m.OnboardingModule) },
];
