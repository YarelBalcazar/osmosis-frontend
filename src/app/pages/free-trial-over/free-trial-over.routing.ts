import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FreeTrialOverComponent } from './free-trial-over.component';

const routes: Routes = [
  {
    path: '', component: FreeTrialOverComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FreeTrialOverRoutingModule { }
