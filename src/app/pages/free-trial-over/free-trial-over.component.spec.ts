import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeTrialOverComponent } from './free-trial-over.component';

describe('FreeTrialOverComponent', () => {
  let component: FreeTrialOverComponent;
  let fixture: ComponentFixture<FreeTrialOverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FreeTrialOverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTrialOverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
