import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FreeTrialOverComponent } from './free-trial-over.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FreeTrialOverRoutingModule } from './free-trial-over.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [FreeTrialOverComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    FreeTrialOverRoutingModule,
    NgbModule
  ],
  providers: []
})
export class FreeTrialOverModule { }
