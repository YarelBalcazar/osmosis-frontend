import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { ResetPasswordService } from './services/reset-password.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private ResetPasswordService: ResetPasswordService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
  ) {  }

  ResetPassForm = this.fb.group({
    email: ['', Validators.required]
    });
  send = false;
  sended = false;
  ngOnInit(): void {

  }

  goToFMU(): void{
    this.router.navigate(['/auth/find-my-url']);
  }

  async resetPassFormSubmit(){
    this.spinnerService.show();
    const emailValidate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.ResetPassForm.controls.email.value);
    if (emailValidate){
      try {
        await this.ResetPasswordService.resetPassword(this.ResetPassForm.controls.email.value).toPromise();
        this.sended = true;
        this.send = true;

      } catch (error) {
        this.sended = true;
        this.send = false;
      }
      this.spinnerService.hide();

    }else{
        this.sended = true;
        this.send = false;
        this.spinnerService.hide();
    }
  }
}
