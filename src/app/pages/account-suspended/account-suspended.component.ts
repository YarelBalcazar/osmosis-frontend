import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AccountSuspendedService } from './services/account-suspended.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-account-suspended-password',
  templateUrl: './account-suspended.component.html',
  styleUrls: ['./account-suspended.component.scss']
})
export class AccountSuspendedComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    // tslint:disable-next-line: no-shadowed-variable
    private AccountSuspendedService: AccountSuspendedService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
  ) {  }

  CreditForm = this.fb.group({
    card_number: ['', Validators.required],
    expiry_day: ['', Validators.required],
    expiry_month: ['1', Validators.required],
    expiry_year: ['2021', Validators.required],
    cvv: ['', Validators.required]
    });
  part1 = true;
  part2 = false;
  part3 = false;
  ngOnInit(): void {

  }
  renewNow(): void{
    this.part1 = false;
    this.part2 = true;
    this.part3 = false;
  }
  addNewCard(): void{
    this.part1 = false;
    this.part2 = true;
    this.part3 = false;
  }
  // tslint:disable-next-line: typedef
  async cardSubmit(){
    this.spinnerService.show();
    if (this.CreditForm.status === 'VALID')
    {
      const cardValidate = /^[0-9]*$/.test(this.CreditForm.value.card_number);
      if (cardValidate){
        this.part1 = false;
        this.part2 = false;
        this.part3 = true;
      }else{
        this.CreditForm.controls.card_number.setErrors({ notNumber: true});
        this.CreditForm.markAllAsTouched();
        this.CreditForm.updateValueAndValidity();
      }
    }else{
      this.CreditForm.markAllAsTouched();
      this.CreditForm.updateValueAndValidity();
    }
    this.spinnerService.hide();
  }
}
