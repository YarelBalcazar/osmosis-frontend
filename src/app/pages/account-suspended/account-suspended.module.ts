import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountSuspendedComponent } from './account-suspended.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AccountSuspendedRoutingModule } from './account-suspended.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [AccountSuspendedComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    AccountSuspendedRoutingModule,
    NgbModule
  ],
  providers: []
})
export class AccountSuspendedModule { }
