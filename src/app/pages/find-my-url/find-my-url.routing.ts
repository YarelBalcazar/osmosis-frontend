import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FindMyUrlComponent } from './find-my-url.component';

const routes: Routes = [
  {
    path: '', component: FindMyUrlComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FindMyUrlRoutingModule { }
