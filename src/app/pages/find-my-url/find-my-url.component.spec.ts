import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindMyUrlComponent } from './find-my-url.component';

describe('FindMyUrlComponent', () => {
  let component: FindMyUrlComponent;
  let fixture: ComponentFixture<FindMyUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindMyUrlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindMyUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
