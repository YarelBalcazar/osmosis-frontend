import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindMyUrlComponent } from './find-my-url.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FindMyUrlRoutingModule } from './find-my-url.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [FindMyUrlComponent],
  imports: [
    CommonModule,
    SharedModule,
    FindMyUrlRoutingModule,
    NgbModule
  ],
  providers: []
})
export class FindMyUrlModule { }
