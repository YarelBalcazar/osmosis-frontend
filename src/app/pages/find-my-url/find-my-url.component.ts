import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { FindMyUrlService } from './services/find-my-url.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './find-my-url.component.html',
  styleUrls: ['./find-my-url.component.scss']
})
export class FindMyUrlComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    // tslint:disable-next-line: no-shadowed-variable
    private FindMyUrlService: FindMyUrlService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
  ) {  }

  FMUForm = this.fb.group({
    email: ['', Validators.required]
    });
  send = false;
  sended = false;
  ngOnInit(): void {

  }
  goToResetPassword(): void{
    this.router.navigate(['/auth/reset-password']);
  }
  // tslint:disable-next-line: typedef
  async findMyUrlFormSubmit(){
    this.spinnerService.show();
    const emailValidate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.FMUForm.controls.email.value);
    if (emailValidate){
      try {
        await this.FindMyUrlService.resetPassword(this.FMUForm.controls.email.value).toPromise();
        this.sended = true;
        this.send = true;

      } catch (error) {
        this.sended = true;
        this.send = false;
      }
      this.spinnerService.hide();

    }else{
        this.sended = true;
        this.send = false;
        this.spinnerService.hide();
    }
  }
}
