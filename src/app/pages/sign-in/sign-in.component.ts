import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { SignInService } from './services/sign-in.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-reset-password',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    // tslint:disable-next-line: no-shadowed-variable
    private SignInService: SignInService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
  ) {  }

  SignInForm = this.fb.group({
    uoe: ['', Validators.required],
    password: ['', Validators.required]
    });
  logged = false;
  signin = false;
  ngOnInit(): void {

  }
  goToResetPassword(): void{
    this.router.navigate(['/auth/reset-password']);
  }
  // tslint:disable-next-line: typedef
  async login(){
    this.spinnerService.show();
    try {
        const body = {
          uoe: this.SignInForm.controls.uoe.value,
          password: this.SignInForm.controls.password.value
        };
        const user: any = await this.SignInService.login(body).toPromise();
        this.signin = false;
        this.logged = true;
        alert("User logeado token: "+user.token)
      } catch (error) {
        this.signin = true;
        this.logged = false;
      }
    this.spinnerService.hide();
  }
}
