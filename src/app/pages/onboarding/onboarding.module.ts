import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnboardingComponent } from './onboarding.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OnboardingRoutingModule } from './onboarding.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [OnboardingComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxMaskModule.forRoot(),
    OnboardingRoutingModule,
    NgbModule
  ],
  providers: []
})
export class OnboardingModule { }
