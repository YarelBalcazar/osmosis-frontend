import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { OnboardingService } from './services/onboarding.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-onboarding-password',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    // tslint:disable-next-line: no-shadowed-variable
    private OnboardingService: OnboardingService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
  ) {  }

  CreditForm = this.fb.group({
    });
  page = 1;
  ngOnInit(): void {
    this.page = 1;
  }
  next(i): void{
    if (i > 0){
      this.page += 1;
      if(this.page >= 6){
        this.page = 6;
      }
    }else if (i < 0){
      this.page -= 1;
      if(this.page < 0){
        this.page = 0
      }
    }
  }
  // tslint:disable-next-line: typedef
  async cardSubmit(){
    this.spinnerService.show();
    if (this.CreditForm.status === 'VALID')
    {
      const cardValidate = /^[0-9]*$/.test(this.CreditForm.value.card_number);
      if (cardValidate){
      }else{
        this.CreditForm.controls.card_number.setErrors({ notNumber: true});
        this.CreditForm.markAllAsTouched();
        this.CreditForm.updateValueAndValidity();
      }
    }else{
      this.CreditForm.markAllAsTouched();
      this.CreditForm.updateValueAndValidity();
    }
    this.spinnerService.hide();
  }
}
