import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OnboardingService {
  BASE_URL: string;

  constructor( private http: HttpClient ) {
    this.BASE_URL = environment.API_URL;
  }

  // tslint:disable-next-line: typedef
  resetPassword( email: any ) {
    return this.http.get(`${this.BASE_URL}/crn-controller/find-my-url?email=${email}`).pipe(
      tap(
        data => data,
        error => error
      )
    );
  }

}
