import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CreatePasswordService } from './services/create-password.service';

@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['./create-password.component.scss']
})
export class CreatePasswordComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private CreatePasswordService: CreatePasswordService,
    private spinnerService: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    config: NgbModalConfig,
    private modalService: NgbModal
  ) { 
    config.backdrop = 'static';
    config.keyboard = false;
  }
  @ViewChild('content', {static: false}) modal;

  CreatePassForm = this.fb.group({
    pass: ['', Validators.required],
    pass2: ['', Validators.required]
  });
  lowerCase = false;
  upperCase = false;
  numberCase = false;
  especialChar = false;
  lengthChar = false;
  passMust = false;
  user: string = "";
  pass1Checked = false;
  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.user = params.u;
        if(!params.u)
        { 
          this.router.navigate(['/auth/sign-up']);
        }
      }
    );
  }
  getPass1(): void {
    this.lowerCase = new RegExp("^(?=.*[a-z])").test(this.CreatePassForm.value.pass);
    this.upperCase = new RegExp("^(?=.*[A-Z])").test(this.CreatePassForm.value.pass);
    this.numberCase = new RegExp("^(?=.*[0-9])").test(this.CreatePassForm.value.pass);
    this.especialChar = new RegExp("^(?=.*[!@#\$%\^&\*])").test(this.CreatePassForm.value.pass);
    this.lengthChar = new RegExp("^(?=.{8,})").test(this.CreatePassForm.value.pass);
    this.pass1Checked = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})").test(this.CreatePassForm.value.pass);
  }
  getPass2(): void {
    if(this.pass1Checked)
    { 
      if(this.CreatePassForm.value.pass == this.CreatePassForm.value.pass2)
      {
        this.passMust = true;
      }else{
        this.passMust = false;
      }
    }else{
      this.CreatePassForm.controls.pass2.setValue("");
    }
    
  }
  createPassFormSubmit(): void{
    this.spinnerService.show();
    if( this.pass1Checked && this.passMust )
    {
      this.CreatePasswordService.createPassword(this.user,this.CreatePassForm.value.pass).subscribe((val)=>{
        if(val.status === 200){
          this.openModal(this.modal);
        }
      });
    }
    this.spinnerService.hide();
  }
  openModal(content): void {
    this.modalService.open(content, { centered: true});
  }
  login(): void {
    this.router.navigate(['/auth/sign-up']); //cambiar a login 
  }
}
