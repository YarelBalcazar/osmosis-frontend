import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreatePasswordComponent } from './create-password.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreatePasswordRoutingModule } from './create-password.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [CreatePasswordComponent],
  imports: [
    CommonModule,
    SharedModule,
    CreatePasswordRoutingModule,
    NgbModule
  ],
  providers: []
})
export class CreatePasswordModule { }
