import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AccountForm } from './models/accountForm';
import { SignUpForm as SignUp } from './models/signUpForm';
import { SettingsForm } from './models/settingsForm';
import { CompleteForm } from './models/completeForm';
import { SignUpService } from './services/sign-up.service';
import { SpinnerService } from 'src/app/core/services/spinner.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit, AfterViewInit {
  progressForm = 0;
  constructor(
    private fb: FormBuilder,
    private signupService: SignUpService,
    private spinnerService: NgxSpinnerService,
    private modalService: NgbModal,
    config: NgbModalConfig,
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }
  AccountForm = this.fb.group({
    fname: ['', Validators.required],
    lname: ['', Validators.required],
    bemail: ['', Validators.required],
    bname: ['', Validators.required]
  });
  SettingForm = this.fb.group({
    username: ['', Validators.required],
    url: ['', Validators.required],
  });
  CompleteForm = this.fb.group({
    tyc: ['', Validators.required],
    coupon: [],
  });
  ObjectForm: SignUp = new SignUp();
  urlSuggested = false;
  accountForm = false;
  settingsForm = false;
  completeForm = false;
  emailValid = true;
  resendmail = false;

  @ViewChild('content', {static: false}) modal;
  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
  }

  accountSubmit(): void{
    const bname = this.AccountForm.controls.bname;
    const fname = this.AccountForm.controls.fname;
    const lname = this.AccountForm.controls.lname;

    if (bname.valid && fname.valid && lname.valid && this.emailValid){
      this.spinnerService.show();
      const account: AccountForm = {
        bname: bname.value,
        fname: fname.value,
        lname: lname.value,
        bemail: this.AccountForm.controls.bemail.value
      };
      this.ObjectForm.account = account;
      this.signupService.verifyEmailBusiness(this.AccountForm.controls.bemail.value).subscribe((val) => {
        if ( !val.body.response.exists )
        {
          this.progressForm = 1;
          this.accountForm = true;
        }else{
          this.AccountForm.controls.bemail.setErrors({emailUsed: true});
          this.AccountForm.controls.bemail.markAsTouched();
        }
        this.spinnerService.hide();
      });

    }else{
      this.AccountForm.markAllAsTouched();

    }
  }
  settingsSubmit(): void{
    const username = this.SettingForm.controls.username;
    const url = this.SettingForm.controls.url;

    if (username.valid && url.valid){
      const settings: SettingsForm = {
        username: username.value,
        url: url.value
      };
      this.signupService.verifyUsername(username.value).subscribe((val) => {
        if ( !val.body.response.exists )
        {
          this.signupService.verifyUrl(url.value).subscribe((val2) => {
            if ( !val2.body.response.exists )
            {
              this.progressForm = 2;
              this.settingsForm = true;
              this.ObjectForm.settings = settings;
            }else{
              this.SettingForm.controls.url.setErrors({urlUsed: true});
              this.SettingForm.controls.url.markAsTouched();
            }
          });
          this.spinnerService.hide();

        }else{
          this.SettingForm.controls.username.setErrors({usernameUsed: true});
          this.SettingForm.controls.username.markAsTouched();
          this.spinnerService.hide();
        }
      });
    }else{
      this.SettingForm.markAllAsTouched();

    }
  }
  async completeSubmit(): Promise<void>{
    const tyc = this.CompleteForm.controls.tyc;
    const coupon = this.CompleteForm.controls.coupon;

    if (tyc.valid && coupon.valid){
      this.progressForm = 3;
      this.completeForm = true;
      const complete: CompleteForm = {
        tyc: tyc.value,
        coupon: coupon.value
      };
      this.ObjectForm.complete = complete;
      this.spinnerService.show();
      this.signupService.signup( this.ObjectForm ).toPromise();
      setTimeout( () => {
        this.spinnerService.hide();
        this.openModal(this.modal);
      }, 2000);

    }else{
      this.CompleteForm.markAllAsTouched();

    }
  }
  backBtn(): void{
    this.progressForm -= 1;
  }
  reSendMail(): void{
    this.spinnerService.show();

    this.signupService.resendEmail(this.AccountForm.controls.bemail.value).subscribe((val) => {
      if ( val.body.response.result )
      {
        this.resendmail = true;
      }else{
        this.resendmail = false;
      }
      this.spinnerService.hide();
    });
  }
  verifyEmail(): void{
    const emailValidate = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.AccountForm.value.bemail);
    if (!emailValidate){
      this.emailValid = false;
    }else{
      this.emailValid = true;
    }
  }

  openModal(content): void {
    this.modalService.open(content, { centered: true});
  }
}
