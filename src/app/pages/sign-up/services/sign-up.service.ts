import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {
  BASE_URL: string;

  constructor( private http: HttpClient ) {
    this.BASE_URL = environment.API_URL;
  }

  signup( body: any ): Observable<any> {
    return this.http.post(`${this.BASE_URL}/sign-up`, body, {
      observe: 'response'
    });
  }

  verifyEmailBusiness( email: any ): Observable<any> {
    return this.http.get(`${this.BASE_URL}/sign-up/verify-business-email?email=${email}`, {
      observe: 'response'
    });
  }

  resendEmail( email: any ): Observable<any> {
    return this.http.get(`${this.BASE_URL}/sign-up/resend-email?email=${email}`, {
      observe: 'response'
    });
  }

  verifyUrl( url: any ): Observable<any> {
    return this.http.get(`${this.BASE_URL}/sign-up/verify-url?url=${url}`, {
      observe: 'response'
    });
  }

  verifyUsername( username: any ): Observable<any> {
    return this.http.get(`${this.BASE_URL}/sign-up/verify-username?username=${username}`, {
      observe: 'response'
    });
  }

}
