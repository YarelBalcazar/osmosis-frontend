export interface AccountForm {
    fname: string;
    lname: string;
    bname: string;
    bemail: string;
}
