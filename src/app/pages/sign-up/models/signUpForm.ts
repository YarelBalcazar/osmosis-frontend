import { AccountForm } from './accountForm';
import { SettingsForm } from './settingsForm';
import { CompleteForm } from './completeForm';
export class SignUpForm {
    account?: AccountForm;
    settings?: SettingsForm;
    complete?: CompleteForm;
}
